package br.com.itau.cliente.service;

import br.com.itau.cliente.exceptions.ClienteNotFoundException;
import br.com.itau.cliente.models.Cliente;
import br.com.itau.cliente.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    ClienteRepository clienteRepository;

    public Cliente create(Cliente cliente) {
        return clienteRepository.save(cliente);
    }

    public Cliente getById(Long id) {
        Optional<Cliente> byId = clienteRepository.findById(id);

        if(!byId.isPresent()) {
            throw new ClienteNotFoundException();
        }

        return byId.get();
    }


}
