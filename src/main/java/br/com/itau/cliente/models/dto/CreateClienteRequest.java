package br.com.itau.cliente.models.dto;

public class CreateClienteRequest {

    private String nome;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

}
