package br.com.itau.cliente.models.dto;

import br.com.itau.cliente.models.Cliente;
import org.springframework.stereotype.Component;

@Component
public class ClienteMapper {

    public Cliente toCliente(CreateClienteRequest createClienteRequest) {
        Cliente customer = new Cliente();
        customer.setNome(createClienteRequest.getNome());
        return customer;
    }

}
