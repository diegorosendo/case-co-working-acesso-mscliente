package br.com.itau.cliente.controller;

import br.com.itau.cliente.models.Cliente;
import br.com.itau.cliente.models.dto.ClienteMapper;
import br.com.itau.cliente.models.dto.CreateClienteRequest;
import br.com.itau.cliente.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    @Autowired
    private ClienteMapper mapper;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Cliente create(@RequestBody CreateClienteRequest createClienteRequest) {
        Cliente cliente = mapper.toCliente(createClienteRequest);

        cliente = clienteService.create(cliente);

        return cliente;
    }

    @GetMapping("/{id}")
    public Cliente getById(@PathVariable Long id) {
        return clienteService.getById(id);
    }



}
